/**
 * 
 */
/**
 * @author Iv�n Castilla
 *
 */
module modelPRM {
	requires simkit;
	requires transitive owlapi.distribution;
	requires org.slf4j;
	requires org.apache.commons.rdf.api;
	requires java.xml;
	exports es.ull.iis.simulation.modelPRM;
}