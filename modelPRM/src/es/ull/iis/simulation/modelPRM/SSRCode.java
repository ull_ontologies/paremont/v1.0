package es.ull.iis.simulation.modelPRM;

public enum SSRCode {
	CODE_BLND("BLND"),
	CODE_DEAF("DEAF"),
	CODE_DPNA("DPNA"),
	CODE_ESAN("ESAN"),
	CODE_EXST("EXST"),
	CODE_MAAS("MAAS"),
	CODE_MEDA("MEDA"),
	CODE_OXYG("OXYG"),
	CODE_PETC("PETC"),
	CODE_PNUT("PNUT"),
	CODE_PPOC("PPOC"),
	CODE_STCR("STCR"),
	CODE_SVAN("SVAN"),
	CODE_WCBD("WCBD"),
	CODE_WCBW("WCBW"),
	CODE_WCHC("WCHC"),
	CODE_WCHP("WCHP"),
	CODE_WCHR("WCHR"),
	CODE_WCHS("WCHS"),
	CODE_WCMP("WCMP"),
	CODE_WCOB("WCOB");

	/** Short name of the instance */
	private final String shortName;

	private SSRCode(String shortName) {
		this.shortName = shortName;
	}
	
	public String getShortName() {
		return shortName;
	}
}