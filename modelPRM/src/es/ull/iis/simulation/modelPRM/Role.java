package es.ull.iis.simulation.modelPRM;

import java.util.ArrayList;

public enum Role {
	AGENT("Agent", 5),
	APRON_COORDINATOR("ApronCoordinator", 1),
	DRIVER("Driver", 1),
	SUPERVISOR("Supervisor", 1);
	
	/** Name of the individual in the ontology */
	private final String shortName;
	/** Number of human resources with role */ 
	private final int minNumberPerShift;
	/** A set with the team members that may potentially assume this role */
	private final ArrayList<String> potentialTeamMembers;
	
	private Role(final String shortName, final int minNumberPerShift) {
		this.shortName = shortName;
		this.minNumberPerShift = minNumberPerShift;
		this.potentialTeamMembers = new ArrayList<>();
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public int getMinNumberPerShift() {
		return minNumberPerShift;
	}
	
	/**
	 * @return the potentialTeamMembers
	 */
	public ArrayList<String> getPotentialTeamMembers() {
		return potentialTeamMembers;
	}

	public void addPotentialTeamMember(String name) {
		potentialTeamMembers.add(name);
	}
}