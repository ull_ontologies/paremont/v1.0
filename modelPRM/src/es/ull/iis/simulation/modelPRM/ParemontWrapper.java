/**
 * 
 */
package es.ull.iis.simulation.modelPRM;

import java.io.File;
import java.util.ArrayList;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * @author Iván Castilla
 *
 */
public class ParemontWrapper extends OWLOntologyWrapper {
	public ParemontWrapper(File file) throws OWLOntologyCreationException {
		super(file);
	}

	public ParemontWrapper(String path) throws OWLOntologyCreationException {
		super(path);
	}
	
	public static enum Clazz {
		ACCESS_TYPE("AccessType"),
		ADAPTED_BUS("AdaptedBus"),
		ADAPTED_VAN("AdaptedVan"),
		AIRLINE_OFFICE("AirlineOffice"),
		AIRPORT("Airport"),
		AIRPORT_APRON_AREA("AirportApronArea"),
		AIRPORT_FACILITY("AirportFacility"),
		AIRPORT_FACILITY_CHANGE("AirportFacilityChange"),
		AIRPORT_FACILITY_FLIGHT("AirportFacilityFlight"),
		AIRPORT_TRANSFER_PHASE("AirportTransferPhase"),
		AIRSIDE_AREA("AirsideArea"),
		AISLE_WHEELCHAIR("AisleWheelchair"),
		AMBULIFT("Ambulift"),
		AMBULIFT_DOCK("AmbuliftDock"),
		AREA("Area"),
		ARRIVAL_SERVICE("ArrivalService"),
		ARRIVALS_AREA("ArrivalsArea"),
		AUTOMATIC_DOOR("AutomaticDoor"),
		AUTOMATIC_STAIRS("AutomaticStairs"),
		BAGGAGE_CLAIM_AREA("BaggageClaimArea"),
		BAGGAGE_CLAIM_BELT("BaggageClaimBelt"),
		BAGGAGE_CLAIM_PHASE("BaggageClaimPhase"),
		BAGGAGE_INCIDENT("BaggageIncident"),
		BOARDING_INCIDENT("BoardingIncident"),
		BOARDING_PHASE("BoardingPhase"),
		BOOKING("Booking"),
		BUGGY("Buggy"),
		BUS_STOP("BusStop"),
		CANCELLATION("Cancellation"),
		CHECK_IN_AREA("CheckInArea"),
		CHECK_IN_COUNTER("CheckInCounter"),
		CHECK_IN_INCIDENT("CheckInIncident"),
		CHECK_IN_PHASE("CheckInPhase"),
		CODE("Code"),
		CRUTCHES("Crutches"),
		CUSTOM_CONTROL_AREA("CustomControlArea"),
		CUSTOMS_CONTROL_PHASE("CustomsControlPhase"),
		DEBOARDING_INCIDENT("DeboardingIncident"),
		DEBOARDING_PHASE("DeboardingPhase"),
		DELAY("Delay"),
		DELAY_CODE("DelayCode"),
		DEPARTURE_SERVICE("DepartureService"),
		DEPARTURES_AREA("DeparturesArea"),
		DETOUR("Detour"),
		END_PHASE("EndPhase"),
		EQUIPMENT("Equipment"),
		EQUIPMENT_STATUS("EquipmentStatus"),
		INCIDENT("Incident"),
		FLIGHT("Flight"),
		FLIGHT_INCIDENT("FlightIncident"),
		FLIGHT_SCOPE("FlightScope"),
		FLIGHT_STATUS("FlightStatus"),
		GATE("Gate"),
		GATES_AREA("GatesArea"),
		HEALTH_INCIDENT("HealthIncident"),
		INFORMATION_OFFICE("InformationOffice"),
		JETBRIDGE("Jetbridge"),
		LANDSIDE_AREA("LandsideArea"),
		LEAVE_AIRPORT_PHASE("LeaveAirportPhase"),
		LIFT("Lift"),
		LOCATION("Location"),
		MISSED_FLIGHT("MissedFlight"),
		MOBILITY_ASSISTIVE_DEVICE("MobilityAssistiveDevice"),
		OWLLIST("OWLList"),
		PRM("PRM"),
		PRMLATE_ARRIVAL("PRMLateArrival"),
		PRMMEETING_POINT("PRMMeetingPoint"),
		PARKING_AREA("ParkingArea"),
		PASSENGER_SERVICES_AREA("PassengerServicesArea"),
		PASSPORT_CONTROL("PassportControl"),
		PASSPORT_CONTROL_AREA("PassportControlArea"),
		PASSPORT_CONTROL_PHASE("PassportControlPhase"),
		PAUSED_PHASE("PausedPhase"),
		PEDIATRIC_WHEELCHAIR("PediatricWheelchair"),
		PERSON("Person"),
		POWERED_WHEELCHAIR("PoweredWheelchair"),
		PRE_BOARDING_COUNTER("PreBoardingCounter"),
		PRE_BOARDING_PHASE("PreBoardingPhase"),
		PUBLIC_PARKING("PublicParking"),
		PUBLIC_TRANSPORT_AREA("PublicTransportArea"),
		RESTAURANT("Restaurant"),
		ROLE("Role"),
		SSRCODE("SSRCode"),
		SECURITY_CONTROL("SecurityControl"),
		SECURITY_CONTROL_AREA("SecurityControlArea"),
		SECURITY_CONTROL_PHASE("SecurityControlPhase"),
		SECURITY_INCIDENT("SecurityIncident"),
		SERVICE("Service"),
		SERVICE_INCIDENT("ServiceIncident"),
		SERVICE_PHASE("ServicePhase"),
		SERVICE_ROUTE("ServiceRoute"),
		SERVICE_STATUS("ServiceStatus"),
		SHOP("Shop"),
		STAIR_CLIMBING_WHEELCHAIR("StairClimbingWheelchair"),
		STAND("Stand"),
		STANDARD_WHEELCHAIR("StandardWheelchair"),
		STANDS_AREA("StandsArea"),
		START_PHASE("StartPhase"),
		STATUS("Status"),
		STRETCHER("Stretcher"),
		SUBWAY_STATION("SubwayStation"),
		TAXI_STOP("TaxiStop"),
		TEAM_MEMBER("TeamMember"),
		TEAM_MEMBER_STATUS("TeamMemberStatus"),
		TEMPORAL_STATUS("TemporalStatus"),
		TERMINAL_SHUTTLE_STOP("TerminalShuttleStop"),
		THING("Thing"),
		TOILET("Toilet"),
		TRAIN_STATION("TrainStation"),
		TRANSFER_BETWEEN_TERMINAL_PHASE("TransferBetweenTerminalPhase"),
		TRANSFER_TO_AIRCRAFT_PHASE("TransferToAircraftPhase"),
		TRANSFER_TO_APRON_ACCESS_DOCK_PHASE("TransferToApronAccessDockPhase"),
		TRANSFER_TO_TERMINAL_PHASE("TransferToTerminalPhase"),
		TRANSIT_PRMTRANSFER_INCIDENT("TransitPRMTransferIncident"),
		TRANSIT_SERVICE("TransitService"),
		UTMCOORDINATE("UTMCoordinate"),
		UTMCOORDINATE_LIST("UTMCoordinateList"),
		VEHICLE("Vehicle"),
		WALKING_STICK("WalkingStick"),
		WHEELCHAIR("Wheelchair"),
		WORK_SHIFT("WorkShift"),
		XXLWHEELCHAIR("XXLWheelchair");
		
		private final String shortName;
		private Clazz(String shortName) {
			this.shortName = shortName;
		}
		/**
		 * @return the shortName
		 */
		public String getShortName() {
			return shortName;
		}
		
	}
	
	public static enum DataProperty {
		HAS_ACTUAL_END_DATE_TIME("hasActualEndDateTime"),
		HAS_ACTUAL_START_DATE_TIME("hasActualStartDateTime"),
		HAS_AIRCRAFT_ID("hasAircraftId"),
		HAS_AIRLINE_CODE("hasAirlineCode"),
		HAS_AIRPORT_CODE("hasAirportCode"),
		HAS_BOOKING_DATE("hasBookingDate"),
		HAS_COMPLAINT_ID("hasComplaintId"),
		HAS_DATUM("hasDatum"),
		HAS_DESCRIPTION("hasDescription"),
		HAS_DURATION("hasDuration"),
		HAS_EASTING("hasEasting"),
		HAS_ID("hasId"),
		HAS_LATITUDE_ZONE("hasLatitudeZone"),
		HAS_LONGITUDE_ZONE("hasLongitudeZone"),
		HAS_NORTHING("hasNorthing"),
		HAS_OWN_WHEEL_CHAIR("hasOwnWheelChair"),
		HAS_STA("hasSTA"),
		HAS_STD("hasSTD"),
		HAS_SATISFACTION_SCORE("hasSatisfactionScore"),
		HAS_SCHEDULED_END_DATE_TIME("hasScheduledEndDateTime"),
		HAS_SCHEDULED_START_DATE_TIME("hasScheduledStartDateTime"),
		HAS_WAITING_PRMTIME("hasWaitingPRMTime"),
		IS_ACCOMPANIED("isAccompanied"),
		IS_ACCOMPANIED_WITH_ASSISTANCE_ANIMAL("isAccompaniedWithAssistanceAnimal"),
		IS_AVAILABLE("isAvailable"),
		IS_ON_CALL("isOnCall"),
		TOP_DATA_PROPERTY("topDataProperty");
		
		private final String shortName;
		private DataProperty(String shortName) {
			this.shortName = shortName;
		}
		/**
		 * @return the shortName
		 */
		public String getShortName() {
			return shortName;
		}
		
	}
	
	public static enum ObjectProperty {
		BELONGS_TO_AIRPORT("belongsToAirport"),
		BELONGS_TO_AREA("belongsToArea"),
		HAS_ACCESS_TYPE("hasAccessType"),
		HAS_ACTIVE_SERVICE_PHASE("hasActiveServicePhase"),
		HAS_AGENT("hasAgent"),
		HAS_AIRPORT_FACILITY("hasAirportFacility"),
		HAS_ALTERNATIVE_DESTINATION_AIRPORT("hasAlternativeDestinationAirport"),
		HAS_ASSIGNED_AIRPORT_FACILITY_FLIGHT("hasAssignedAirportFacilityFlight"),
		HAS_ASSIGNED_AREA("hasAssignedArea"),
		HAS_ASSIGNED_SERVICE("hasAssignedService"),
		HAS_ASSOCIATED_FLIGHT("hasAssociatedFlight"),
		HAS_BOOKING("hasBooking"),
		HAS_CONTENTS("hasContents"),
		HAS_DELAY_CODE("hasDelayCode"),
		HAS_DESTINATION_AIRPORT("hasDestinationAirport"),
		HAS_DRIVER("hasDriver"),
		HAS_END_LOCATION("hasEndLocation"),
		HAS_EQUIPMENT_STATUS("hasEquipmentStatus"),
		HAS_FLIGHT_INCIDENT("hasFlightIncident"),
		HAS_FLIGHT_SCOPE("hasFlightScope"),
		HAS_FLIGHT_STATUS("hasFlightStatus"),
		HAS_LINKED_FLIGHT("hasLinkedFlight"),
		HAS_LIST_PROPERTY("hasListProperty"),
		HAS_LOCATION("hasLocation"),
		HAS_MOBILITY_ASSISTIVE_DEVICE("hasMobilityAssistiveDevice"),
		HAS_NEXT("hasNext"),
		HAS_ORIGIN_AIRPORT("hasOriginAirport"),
		HAS_PRM("hasPRM"),
		HAS_PERIMETER("hasPerimeter"),
		HAS_POTENTIAL_ROLE("hasPotentialRole"),
		HAS_REFERENCE_POINT("hasReferencePoint"),
		HAS_SSRCODE("hasSSRCode"),
		HAS_SERVICE("hasService"),
		HAS_SERVICE_INCIDENT("hasServiceIncident"),
		HAS_SERVICE_PHASE("hasServicePhase"),
		HAS_SERVICE_ROUTE("hasServiceRoute"),
		HAS_SERVICE_STATUS("hasServiceStatus"),
		HAS_START_LOCATION("hasStartLocation"),
		HAS_STATUS("hasStatus"),
		HAS_TEAM_MEMBER_STATUS("hasTeamMemberStatus"),
		HAS_TEMPORAL_STATUS("hasTemporalStatus"),
		HAS_VEHICLE("hasVehicle"),
		HAS_WORK_SHIFT("hasWorkShift"),
		INCLUDES_LOCATION("includesLocation"),
		INVOLVES_ROLE("involvesRole"),
		INVOLVES_WORKSHIFT("involvesWorkshift"),
		IS_ACCESS_TIPE_OF("isAccessTipeOf"),
		IS_ACTIVE_SERVICE_PHASE_OF("isActiveServicePhaseOf"),
		IS_AGENT_OF("isAgentOf"),
		IS_AIRPORT_FACILITY_OF("isAirportFacilityOf"),
		IS_AIRPORT_OF("isAirportOf"),
		IS_ALTERNATIVE_DESTINATION_AIRPORT_OF("isAlternativeDestinationAirportOf"),
		IS_ASSIGNED_AIRPORT_FACILITY_FLIGHT_OF("isAssignedAirportFacilityFlightOf"),
		IS_ASSIGNED_AREA_OF("isAssignedAreaOf"),
		IS_ASSOCIATED_FLIGHT_OF("isAssociatedFlightOf"),
		IS_DELAY_CODE_OF("isDelayCodeOf"),
		IS_DESTINATION_AIRPORT_OF("isDestinationAirportOf"),
		IS_DRIVER_OF("isDriverOf"),
		IS_END_LOCATION_OF("isEndLocationOf"),
		IS_EQUIPMENT_STATUS_OF("isEquipmentStatusOf"),
		IS_FLIGHT_INCIDENT_OF("isFlightIncidentOf"),
		IS_FLIGHT_SCOPE_OF("isFlightScopeOf"),
		IS_FLIGHT_STATUS_OF("isFlightStatusOf"),
		IS_FOLLOWED_BY("isFollowedBy"),
		IS_LOCATION_OF("isLocationOf"),
		IS_MOBILITY_ASSISTIVE_DEVICE_OF("isMobilityAssistiveDeviceOf"),
		IS_ORIGIN_AIRPORT_OF("isOriginAirportOf"),
		IS_PERIMETER_OF("isPerimeterOf"),
		IS_REFERENCE_POINT_OF("isReferencePointOf"),
		IS_ROLE_OF("isRoleOf"),
		IS_SSRCODE_OF("isSSRCodeOf"),
		IS_SERVICE_INCIDENT_OF("isServiceIncidentOf"),
		IS_SERVICE_PHASE_OF("isServicePhaseOf"),
		IS_SERVICE_ROUTE_OF("isServiceRouteOf"),
		IS_SERVICE_STATUS_OF("isServiceStatusOf"),
		IS_START_LOCATION_OF("isStartLocationOf"),
		IS_TEAM_MEMBER_STATUS_OF("isTeamMemberStatusOf"),
		IS_VEHICLE_OF("isVehicleOf"),
		IS_WORK_SHIFT_OF("isWorkShiftOf"),
		TOP_OBJECT_PROPERTY("topObjectProperty");
		
		private final String shortName;
		private ObjectProperty(String shortName) {
			this.shortName = shortName;
		}
		/**
		 * @return the shortName
		 */
		public String getShortName() {
			return shortName;
		}
		
	}

	public void printIndividuals(boolean full) {
		if (full) {
			for (String individual : individualsToString()) {
				final ArrayList<String> props = getIndividualProperties(individual, "\t");
				for (String prop : props)
					System.out.println(individual + "\t" + prop);
			}
		}
		else  {
			for (String individual : individualsToString())
				System.out.println(individual);
		}
	}


	public void printIndividuals(String classIRI, boolean full) {
		if (full) {
			for (String individual : getIndividuals(classIRI)) {
				final ArrayList<String> props = getIndividualProperties(individual, "\t");
				for (String prop : props)
					System.out.println(individual + "\t" + prop);
			}
		}
		else  {
			for (String individual : getIndividuals(classIRI))
				System.out.println(individual);
		}
	}
	
	public void printClasses() {
		for (String clazz: classesToString())
			System.out.println(clazz);
	}
	
	public void printClassesAsEnum() {
		for (String name : classesToString()) {
			System.out.println(camel2SNAKE(name) + "(\"" + name + "\"),");
		}
	}

	public void printDataProperties() {
		for (String dataProp: dataPropertiesToString())
			System.out.println(dataProp);
	}
	
	public void printDataPropertiesAsEnum() {
		for (String name : dataPropertiesToString()) {
			System.out.println(camel2SNAKE(name) + "(\"" + name + "\"),");
		}
	}

	public void printObjectProperties() {
		for (String objectProp: objectPropertiesToString())
			System.out.println(objectProp);
	}
	
	public void printObjectPropertiesAsEnum() {
		for (String name : objectPropertiesToString()) {
			System.out.println(camel2SNAKE(name) + "(\"" + name + "\"),");
		}
	}
	
	public ArrayList<String> getEnglishCommentForClass(IRI iri) {
		final ArrayList<String> list = new ArrayList<>();
//		final OWLClass owlClass = getClass(clazz.getShortName());
		for(OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(iri)) {
		    if(a.getProperty().isComment()) {
		        if(a.getValue() instanceof OWLLiteral) {
		            OWLLiteral val = (OWLLiteral) a.getValue();
		            if (val.hasLang("en"))
		            	list.add(val.getLiteral());
		        }
		    }
		}
		return list;
	}
	
	
	public void printClassesAsLatexTable(int lineWidth) {
		final int classWidth = lineWidth / 4;
		System.out.println("\\begin{table*}[ht!]\r\n"
				+ "\\footnotesize\\sf\\centering\r\n"
				+ "\\begin{tabular}{p{" + classWidth + "cm}p{" + (lineWidth - classWidth) + "cm}}\r\n"
				+ "\\hline");
		for (String name : classesToString()) {
			System.out.println(camel2SNAKE(name) + "(\"" + name + "\"),");
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
//			final ParemontWrapper wrap = new ParemontWrapper("C:/Users/Iván Castilla/Downloads/paremontOld.owl");
			final ParemontWrapper wrap = new ParemontWrapper("resources/paremont_v1.0.owl");
			wrap.printIndividuals(Clazz.TEAM_MEMBER_STATUS.getShortName(), false);

//			wrap.printClassesAsEnum();
//			wrap.printDataPropertiesAsEnum();
//			wrap.printObjectPropertiesAsEnum();
//			for (String  str : wrap.getEnglishCommentForClass(wrap.getObjectProperty(ObjectProperty.BELONGS_TO_AREA.getShortName()).getIRI()))
//				System.out.println(str);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
	}
		
	
}
