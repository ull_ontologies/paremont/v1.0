package es.ull.iis.simulation.modelPRM;

public enum TeamMemberStatus {
	AVAILABLE("Available"),
	BUSY("Busy"),
	ON_CALL("OnCall"),
	OUT_OF_OFFICE("OutOfOffice"),
	OVER_TIME("OverTime"),
	REST("Rest");

	/** Short name of the instance */
	private final String shortName;

	private TeamMemberStatus(String shortName) {
		this.shortName = shortName;
	}
	
	public String getShortName() {
		return shortName;
	}
}