package es.ull.iis.simulation.modelPRM;

public enum Area {
	GATE_A(ParemontWrapper.Clazz.GATE, "GateA", "Gate A", 2),
	GATE_B(ParemontWrapper.Clazz.GATE, "GateB", "Gate B", 1),
	CHECKINA(ParemontWrapper.Clazz.CHECK_IN_AREA, "CheckInA", "Check In A", 2);
	
	/** Name of the original class in the ontology */
	private final ParemontWrapper.Clazz clazz;
	/** Short name of the instance */
	private final String shortName;
	/** Description of the instance */
	private final String description;
	/** Number of agents assigned to this area */
	private final int nAgents;
	
	private Area(final ParemontWrapper.Clazz clazz, final String shortName, final String description, final int nAgents) {
		this.clazz = clazz;
		this.shortName = shortName;
		this.description = description;
		this.nAgents = nAgents;
	}
	
	public ParemontWrapper.Clazz getClazz() {
		return clazz;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public String getDescription() {
		return description;
	}
	
	/**
	 * @return the nAgents
	 */
	public int getnAgents() {
		return nAgents;
	}
}