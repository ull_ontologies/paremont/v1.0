package es.ull.iis.simulation.modelPRM;

import java.util.EnumSet;

public class RoleAssignment {
	private final EnumSet<Role> potentialRoles;
	private final int n;
	/**
	 * @param potentialRoles
	 * @param n
	 */
	public RoleAssignment(EnumSet<Role> potentialRoles, int n) {
		this.potentialRoles = potentialRoles;
		this.n = n;
	}
	/**
	 * @return the potentialRoles
	 */
	public EnumSet<Role> getPotentialRoles() {
		return potentialRoles;
	}
	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}
}