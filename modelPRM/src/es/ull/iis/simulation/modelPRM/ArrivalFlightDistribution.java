package es.ull.iis.simulation.modelPRM;

import simkit.random.DiscreteRandomVariate;

public class ArrivalFlightDistribution {
	private final int startingHour;
	private final int endingHour;
	private final DiscreteRandomVariate nFlights;
	/**
	 * @param startingHour
	 * @param endingHour
	 * @param nFlights
	 */
	public ArrivalFlightDistribution(int startingHour, int endingHour, DiscreteRandomVariate nFlights) {
		this.startingHour = startingHour;
		this.endingHour = endingHour;
		this.nFlights = nFlights;
	}
	/**
	 * @return the startingHour
	 */
	public int getStartingHour() {
		return startingHour;
	}
	/**
	 * @return the endingHour
	 */
	public int getEndingHour() {
		return endingHour;
	}
	/**
	 * @return the nFlights
	 */
	public DiscreteRandomVariate getnFlights() {
		return nFlights;
	}
}