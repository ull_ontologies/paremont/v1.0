/**
 * 
 */
package es.ull.iis.simulation.modelPRM;

import java.io.File;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.io.StreamDocumentTarget;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.model.SetOntologyID;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

/**
 * @author Iván Castilla
 *
 */
public class TestOWLAPI {
	final private static String PATH_EXISTING = "resources/paremontLite_v1.0.owl"; 
	final private static String PATH_NEW = "resources/test.owl"; 

	/**
	 * 
	 */
	public TestOWLAPI() {
		// TODO Auto-generated constructor stub
	}

    /**
     * Prints the IRI of an ontology and its document IRI.
     * 
     * @param manager The manager that manages the ontology
     * @param ontology The ontology
     */
    private static void printOntology(OWLOntologyManager manager, OWLOntology ontology) {
        IRI ontologyIRI = ontology.getOntologyID().getOntologyIRI().get();
        IRI documentIRI = manager.getOntologyDocumentIRI(ontology);
        System.out.println(ontologyIRI == null ? "anonymous" : ontologyIRI.toQuotedString());
        System.out.println(" from " + documentIRI.toQuotedString());
    }
    
    private static void printClasses(OWLOntologyManager manager, OWLOntology ontology) {
    	for(OWLClass cls : ontology.getClassesInSignature()) 
    		System.out.println(cls);    	
    }
    
    
    private static void testModifyIRIandVersion(OWLOntologyManager manager, OWLOntology ontology) {
        // Once an ontology has been created its ontology ID (Ontology IRI and
        // version IRI can be changed) to do this we must apply a SetOntologyID
        // change through the ontology manager. Lets specify a version IRI for
        // our ontology. In our case we will just "extend" our ontology IRI with
        // some version information. We could of course specify any IRI for our
        // version IRI.
    	IRI ontologyIRI = manager.getOntologyDocumentIRI(ontology);
        IRI versionIRI = IRI.create(ontologyIRI + "/version1");
        // Note that we MUST specify an ontology IRI if we want to specify a
        // version IRI
        OWLOntologyID newOntologyID = new OWLOntologyID(ontologyIRI, versionIRI);
        // Create the change that will set our version IRI
        SetOntologyID setOntologyID = new SetOntologyID(ontology, newOntologyID);
        // Apply the change
        manager.applyChange(setOntologyID);
    }

//    private static OWLClass testCreateClass(OWLOntologyManager manager, OWLOntology ontology) {
//        OWLDataFactory factory = manager.getOWLDataFactory();
//        OWLClass newClass = factory.
//    	
//    }
    private static void testCreateAndSaveOntology() {
        // We first need to create an OWLOntologyManager, which will provide a
        // point for creating, loading and saving ontologies. We can create a
        // default ontology manager with the OWLManager class. This provides a
        // common setup of an ontology manager. It registers parsers etc. for
        // loading ontologies in a variety of syntaxes
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        // In OWL 2, an ontology may be named with an IRI (Internationalised
        // Resource Identifier) We can create an instance of the IRI class as
        // follows:
        IRI ontologyIRI = IRI.create("http://www.ull.es/iis/ontologies/testOntology");
        OWLOntology ontology;
		try {
			ontology = manager.createOntology(ontologyIRI);
	        System.out.println("Created ontology: " + ontology);
	        
	        // Now we want to specify that A is a subclass of B. To do this, we add
	        // a subclass axiom. A subclass axiom is simply an object that specifies
	        // that one class is a subclass of another class. We need a data factory
	        // to create various object from. Each manager has a reference to a data
	        // factory that we can use.
	        OWLDataFactory factory = manager.getOWLDataFactory();
	        // Get hold of references to class A and class B. Note that the ontology
	        // does not contain class A or classB, we simply get references to
	        // objects from a data factory that represent class A and class B
	        OWLClass clsA = factory.getOWLClass(IRI.create(ontologyIRI + "#A"));
	        OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI + "#B"));
	        // Now create the axiom
	        OWLAxiom axiom = factory.getOWLSubClassOfAxiom(clsA, clsB);
	        // We now add the axiom to the ontology, so that the ontology states
	        // that A is a subclass of B. To do this we create an AddAxiom change
	        // object. At this stage neither classes A or B, or the axiom are
	        // contained in the ontology. We have to add the axiom to the ontology.
	        AddAxiom addAxiom = new AddAxiom(ontology, axiom);
	        // We now use the manager to apply the change
	        manager.applyChange(addAxiom);
	        
	        PrefixManager pm = new DefaultPrefixManager(null, null, ontologyIRI.toString());
	        // Let's specify the :John has a wife :Mary Get hold of the necessary
	        // individuals and object property
	        OWLNamedIndividual john = factory.getOWLNamedIndividual("John", pm);
	        OWLNamedIndividual mary = factory.getOWLNamedIndividual("Mary", pm);
	        
	        // Now create a ClassAssertion to specify that :Mary is an instance of #A
	        OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(clsA, mary);
	        // Add the class assertion
	        manager.addAxiom(ontology, classAssertion);
	        // Now create a ClassAssertion to specify that :John is an instance of #B
	        classAssertion = factory.getOWLClassAssertionAxiom(clsB, john);
	        // Add the class assertion
	        manager.addAxiom(ontology, classAssertion);
	        OWLObjectProperty hasWife = factory.getOWLObjectProperty("hasWife", pm);
	        // To specify that :John is related to :Mary via the :hasWife property
	        // we create an object property assertion and add it to the ontology
	        OWLObjectPropertyAssertionAxiom propertyAssertion =
	            factory.getOWLObjectPropertyAssertionAxiom(hasWife, john, mary);
	        manager.addAxiom(ontology, propertyAssertion);
	        // Now let's specify that :John is aged 51. Get hold of a data property
	        // called :hasAge
	        OWLDataProperty hasAgeDP = factory.getOWLDataProperty(":hasAge", pm);
	        // To specify that :John has an age of 51 we create a data property
	        // assertion and add it to the ontology
	        OWLDataPropertyAssertionAxiom dataPropertyAssertion =
	            factory.getOWLDataPropertyAssertionAxiom(hasAgeDP, john, 51);
	        manager.addAxiom(ontology, dataPropertyAssertion);
	        
	        OWLXMLDocumentFormat format = new OWLXMLDocumentFormat();
	        manager.saveOntology(ontology, format, new StreamDocumentTarget(System.out));
//	        File file = new File(PATH_NEW);
//	        manager.saveOntology(ontology, IRI.create(file.toURI()));
	        
		} catch (OWLOntologyCreationException | OWLOntologyStorageException e) {
			e.printStackTrace();
		}
    }
    
//    private void testFormatOntology() {
//        // Now save a local copy of the ontology. (Specify a path appropriate to
//        // your setup)
//        // By default ontologies are saved in the format from which they were
//        // loaded. In this case the ontology was loaded from rdf/xml. We
//        // can get information about the format of an ontology from its manager
//        OWLDocumentFormat format = manager.getOntologyFormat(ontology);
//        // We can save the ontology in a different format. Lets save the
//        // ontology
//        // in owl/xml format
//        OWLXMLDocumentFormat owlxmlFormat = new OWLXMLDocumentFormat();
//        // Some ontology formats support prefix names and prefix IRIs. In our
//        // case we loaded the Koala ontology from an rdf/xml format, which
//        // supports prefixes. When we save the ontology in the new format we
//        // will copy the prefixes over so that we have nicely abbreviated IRIs
//        // in the new ontology document
//        if (format.isPrefixOWLOntologyFormat()) {
//            owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
//        }
//        manager.saveOntology(ontology, owlxmlFormat, IRI.create(file.toURI()));
//        // We can also dump an ontology to System.out by specifying a different
//        // OWLOntologyOutputTarget. Note that we can write an ontology to a
//        // stream in a similar way using the StreamOutputTarget class
//        // Try another format - The Manchester OWL Syntax
//        ManchesterSyntaxDocumentFormat manSyntaxFormat = new ManchesterSyntaxDocumentFormat();
//        if (format.isPrefixOWLOntologyFormat()) {
//            manSyntaxFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
//        }
//        // Replace the ByteArrayOutputStream wth an actual output stream to save
//        // to a file.
//        manager.saveOntology(ontology, manSyntaxFormat,
//            new StreamDocumentTarget(new ByteArrayOutputStream()));
//    }
    
    private static void testReadOntology() {
		  OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		  try {
			OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(PATH_EXISTING));
	        OWLDocumentFormat format = manager.getOntologyFormat(ontology);
	        System.out.println(format);
			printOntology(manager, ontology);
			printClasses(manager, ontology);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
    }
    
    private static void testListInstances() {
    	OWLOntologyWrapper ont;
		try {
			ont = new OWLOntologyWrapper(PATH_EXISTING);
	    	for (String status : ont.getIndividuals(ParemontWrapper.Clazz.TEAM_MEMBER_STATUS.getShortName()))
	    		System.out.println(status);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		testCreateAndSaveOntology();
//		testReadOntology();
		testListInstances();
	}

}
