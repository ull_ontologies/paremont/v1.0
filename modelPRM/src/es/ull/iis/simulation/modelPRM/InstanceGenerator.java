/**
 * 
 */
package es.ull.iis.simulation.modelPRM;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Set;
import java.util.TreeMap;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import simkit.random.DiscreteRandomVariate;
import simkit.random.RandomNumber;
import simkit.random.RandomNumberFactory;
import simkit.random.RandomVariate;
import simkit.random.RandomVariateFactory;

/**
 * <p>This class is intended to generate instances that support the response to the following competency questions:</p>
 * <ul>
 * <li><b>QP04:</b> Which <i>Service</i> <i>Bookings</i> has been made, at least, 48 hours in advance, for a period of time?</li>
 * <li><b>QO8:</b> Which <i>Agents</i> are currently working at a specific <i>Area</i> and have an <i>Available</i> <i>TeamMemberStatus</i>?</li>
 * <li><b>QR3:</b> Which <i>TeamMembers</i> with a specific <i>Role</i> who are on call, have been activated for a period of time?</li>
 * <li><b>QQ12:</b> Which <i>Flights</i> has been delayed because of <i>Code #19</i>, and which <i>Services</i> are assigned to such <i>Flight</i>, for a period of time?</li>
 * </ul>
 * 
 * <p>Instances represent a 15-days temporal framework, with operative timetable from 6:00 to 22:00 (two 8-hour shifts).</p>
 * 
 * <p>This data is used as follows:</p>
 * <ul>
 * <li>3 days as historical data</li>
 * <li>One operations day</li>
 * <li>3 days as planning</li>
 * </ul>
 * 
 * <p>In short, we have to create:</p>
 * <ul>
 * <li>
 *     <b>Individuals</b>
 *     <ul>
 *     <li><i>Area:</i> Gates A (<i>GatesArea</i>), Gates B (<i>GatesArea</i>), Check In A (<i>CheckInArea</i>)</li>
 *     <li>32 <i>Person--&gtTeamMember</i> Everyone can perform the <i>Agent</i> role, and 1/4 are also either <i>Driver</i>, <i>Supervisor</i> or <i>ApronCoordinador</i></li>
 *     </ul>
 * </li>
 * <li>
 *     <b>For each day</b>
 *     <ul>
 *     <li>There are X <i>Flights</i> distributed through the day (X/2 <i>ArrivalFlight</i> and X/2 <i>DepartureFlight</i>)</li>
 *     <li>For each arrival flight, there is a departure flight one hour later
 *     <li>A proportion of <i>Departure Flights</i> have <i>Delays</i> with different <i>Codes</i> (currently we are only assigning code #19). <i>Delays</i> are 1-30 minutes</li>
 *     <li>Each <i>Flight</i> requires from 0 to 3 <i>Services</i></li>
 *     <li>A proportion of <i>Services</i> are booked in advance. Booking date follows an exponential distribution with average 5 days in advance</li>
 *     <li>There are 5 <i>Agents</i>, one <i>Driver</i>, one <i>Supervisor</i> and one <i>ApronCoordinador</i> per work shift</li>
 *     <li>There is one extra team member per role and shift, who is on call</li>
 *     </ul>
 * </li>
 * </ul>

 * @author Iván Castilla
 *
 */
public class InstanceGenerator {
	//	private static class PRM {
//		private final String SSRCode;
//	}
	private final static String PATH = "resources/paremont_v1.0.owl";
	
	private final static String PREFIX_TEAM_MEMBER = "TM_";
	private final static String PREFIX_WORKSHIFT = "WS_";
	private final static String PREFIX_FLIGHT = "FL_";
	private final static String PREFIX_DELAY = "DEL_";
	private final static String PREFIX_SERVICE = "SERV_";
	private final static String PREFIX_PRM = "PRM_";
	private final static String PREFIX_BOOKING = "BOOK_";
	private final static String PREFIX_STATUS = "STAT_";
	/** Time horizon to generate instances (in days) */
	private final static int DEF_TOTAL_DAYS = 3;
	/** Opening hour for the airport every day */
	private final static int DEF_STARTING_HOUR = 6;
	/** Closing hour for the airport every day */
	private final static int DEF_ENDING_HOUR = 22;
	/** Working shifts at the airport every day, expressed as pairs &ltstarting hour, finishing hour&gt  */
	private final static int[][] DEF_SHIFTS = new int[][]{{DEF_STARTING_HOUR, DEF_STARTING_HOUR + 8}, {DEF_ENDING_HOUR - 8, DEF_ENDING_HOUR}};
	/** Proportion of delayed flights from the total of daily flights. Default: 1 out of 20 */ 
	private final static double PROP_DELAYED_FLIGHTS = 1.0 / 20.0;
	/** Probabilistic distribution to define the delay time, in minutes */ 
	private final static DiscreteRandomVariate RND_DELAY_TIME = (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 1, 30);
	/** Probabilistic distribution to define the number of daily flights */ 
	private final static DiscreteRandomVariate RND_N_SERVICES_PER_FLIGHT = (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 0, 3);
	/** Probabilistic distribution to select a SSR Code */ 
	private final static DiscreteRandomVariate RND_SSR_CODE = (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 0, SSRCode.values().length - 1);
	/** Proportion of services that are booked in advance. Default: 70% */ 
	private final static double RND_PROP_BOOKED_SERVICES = 0.7;
	/** Probabilistic distribution to define the amount of days that the service is booked in advance (when booked) */ 
	private final static RandomVariate RND_DAYS_BEFORE_BOOKING_SERVICES = RandomVariateFactory.getInstance("ExponentialVariate", 5);
	/** Proportion of on-call team members who are activated. Default: 10% */ 
	private final static double PROP_ACTIVATED_ON_CALL = 0.1;
	/** Proportion of a workshift when a team member is available. Default: 12.5% */ 
	private final static double PROP_AVAILABLE_STATUS = 0.125;
	/** Time interval, in minutes, used to define the status of a team member */
	private final static int STATUS_TIME_INTERVAL = 30;
	private final static ArrayList<RoleAssignment> POTENTIAL_ROLES = new ArrayList<>(); 
	private final static ArrayList<ArrivalFlightDistribution> ARRIVAL_FLIGHTS = new ArrayList<>();
	
	static {
		// Define the staff, i.e. how many team members have any combination of roles
		// Currently, each team member can only be one type to make easier the construction of work shifts
		// We assume that we have enough resources for 4 consecutive shifts, including an extra team member on call per shift
		POTENTIAL_ROLES.add(new RoleAssignment(EnumSet.of(Role.AGENT), (Role.AGENT.getMinNumberPerShift() + 1) * 4)); 		
		POTENTIAL_ROLES.add(new RoleAssignment(EnumSet.of(Role.DRIVER), (Role.DRIVER.getMinNumberPerShift() + 1) * 4));
		POTENTIAL_ROLES.add(new RoleAssignment(EnumSet.of(Role.APRON_COORDINATOR), (Role.APRON_COORDINATOR.getMinNumberPerShift() + 1) * 4));
		POTENTIAL_ROLES.add(new RoleAssignment(EnumSet.of(Role.SUPERVISOR), (Role.SUPERVISOR.getMinNumberPerShift() + 1) * 4));
		
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(6, 8, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 2, 5)));
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(8, 11, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 8, 12)));
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(11, 14, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 4, 7)));
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(14, 17, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 4, 7)));
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(17, 20, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 8, 12)));
		ARRIVAL_FLIGHTS.add(new ArrivalFlightDistribution(20, 22, (DiscreteRandomVariate)RandomVariateFactory.getInstance("DiscreteUniformVariate", 1, 4)));
	}

	private final ParemontWrapper owlWrapper;
	private final RandomNumber rnd;
	private final String airportName;
	
	/**
	 * 
	 */
	public InstanceGenerator(String airportName) throws OWLOntologyCreationException {
		owlWrapper = new ParemontWrapper(PATH);
		rnd = RandomNumberFactory.getInstance();
		this.airportName = airportName;
		owlWrapper.addIndividual(ParemontWrapper.Clazz.AIRPORT.getShortName(), airportName);
	}

	/**
	 * @return the owlWrapper
	 */
	public ParemontWrapper getOwlWrapper() {
		return owlWrapper;
	}

	public void createAreas() {
		for (Area area : Area.values()) {
			owlWrapper.addIndividual(area.getClazz().getShortName(), area.getShortName());
			owlWrapper.addDataPropertyValue(area.getShortName(), ParemontWrapper.DataProperty.HAS_DESCRIPTION.getShortName(), area.getDescription());
			owlWrapper.addDataPropertyValue(area.getShortName(), ParemontWrapper.DataProperty.HAS_ID.getShortName(), area.toString());
			owlWrapper.addDataPropertyValue(area.getShortName(), ParemontWrapper.DataProperty.IS_AVAILABLE.getShortName(), "true", OWL2Datatype.XSD_BOOLEAN);
			owlWrapper.addObjectPropertyValue(area.getShortName(), ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
		}
	}
	
	public void createTeamMembers() {
		int tmCounter = 0;
		for (RoleAssignment assignment : POTENTIAL_ROLES) {
			for (int i = 0; i < assignment.getN(); i++) {
				final String tmName = PREFIX_TEAM_MEMBER + tmCounter++;
				owlWrapper.addIndividual(ParemontWrapper.Clazz.TEAM_MEMBER.getShortName(), tmName);
				owlWrapper.addObjectPropertyValue(tmName, ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
				for (Role role : assignment.getPotentialRoles()) {
					owlWrapper.addObjectPropertyValue(tmName, ParemontWrapper.ObjectProperty.HAS_POTENTIAL_ROLE.getShortName(), role.getShortName());
					role.addPotentialTeamMember(tmName);
				}
			}			
		}
	}
	
	private TeamMemberStatus getNextStatus(int nTimeIntervals, int intervalCounter, int restInterval) {
		if (intervalCounter >= nTimeIntervals)
			return null;
		if (restInterval == intervalCounter)
			return TeamMemberStatus.REST;
		double rndSample = rnd.draw();
		if (rndSample < PROP_AVAILABLE_STATUS) {
			return TeamMemberStatus.AVAILABLE;
		}
		return TeamMemberStatus.BUSY;
	}
	
	private void createTemporalStatus(String statName, String wsName, String tmName, TeamMemberStatus status, String startDate, String endDate) {
		owlWrapper.addIndividual(ParemontWrapper.Clazz.TEMPORAL_STATUS.getShortName(), statName);
		owlWrapper.addDataPropertyValue(statName, ParemontWrapper.DataProperty.HAS_ACTUAL_START_DATE_TIME.getShortName(), startDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(statName, ParemontWrapper.DataProperty.HAS_ACTUAL_END_DATE_TIME.getShortName(), endDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addObjectPropertyValue(statName, ParemontWrapper.ObjectProperty.HAS_STATUS.getShortName(), status.getShortName());		
		owlWrapper.addObjectPropertyValue(wsName, ParemontWrapper.ObjectProperty.HAS_TEMPORAL_STATUS.getShortName(), statName);
		owlWrapper.addObjectPropertyValue(tmName, ParemontWrapper.ObjectProperty.HAS_TEMPORAL_STATUS.getShortName(), statName);		
	}
	
	private void populateTeamMemberStatus(String wsName, String tmName, Role role, Instant refInstant, int day, int[] shiftHours, boolean onCall) {
		final int nTimeIntervals = (shiftHours[1] - shiftHours[0]) * 60 / STATUS_TIME_INTERVAL;
		// The team member is not on call, or he/she becomes active 
		if (!onCall || (rnd.draw() < PROP_ACTIVATED_ON_CALL)) {
			// We define one time interval in "REST" status
			final int restInterval = (int) (rnd.draw() * nTimeIntervals);
			int startInterval = 0;
			int statusCounter = 0;
			TeamMemberStatus currentStatus = getNextStatus(nTimeIntervals, 0, restInterval);
			for (int i = 0; i < nTimeIntervals; i++) {
				final TeamMemberStatus nextStatus = getNextStatus(nTimeIntervals, i + 1, restInterval);
				if (!currentStatus.equals(nextStatus)) {
					final int startHour = shiftHours[0] + startInterval * STATUS_TIME_INTERVAL / 60;
					final int startMinute = (startInterval * STATUS_TIME_INTERVAL) % 60;
					final int endHour = shiftHours[0] + (i + 1) * STATUS_TIME_INTERVAL / 60;
					final int endMinute = ((i + 1) * STATUS_TIME_INTERVAL) % 60;
					createTemporalStatus(PREFIX_STATUS + wsName + "_" + statusCounter++, wsName, tmName, currentStatus, adjustDateTime(refInstant, day, startHour, startMinute).toString(), adjustDateTime(refInstant, day, endHour, endMinute).toString());
					currentStatus = nextStatus;
					startInterval = i + 1; 
				}
			}
		}
		else {
			createTemporalStatus(PREFIX_STATUS + wsName, wsName, tmName, TeamMemberStatus.ON_CALL, adjustDateTime(refInstant, day, shiftHours[0]).toString(), adjustDateTime(refInstant, day, shiftHours[1]).toString());
		}	
	}

	private void createWorkShift(String wsName, String tmName, Role role, Instant refInstant, int day, int[] shiftHours, boolean onCall) {
		final String startDate = adjustDateTime(refInstant, day, shiftHours[0]).toString();
		final String endDate = adjustDateTime(refInstant, day, shiftHours[1]).toString();
		owlWrapper.addIndividual(ParemontWrapper.Clazz.WORK_SHIFT.getShortName(), wsName);
		if (onCall)
			owlWrapper.addDataPropertyValue(wsName, ParemontWrapper.DataProperty.IS_ON_CALL.getShortName(), "true", OWL2Datatype.XSD_BOOLEAN);
		owlWrapper.addDataPropertyValue(wsName, ParemontWrapper.DataProperty.HAS_SCHEDULED_START_DATE_TIME.getShortName(), startDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(wsName, ParemontWrapper.DataProperty.HAS_SCHEDULED_END_DATE_TIME.getShortName(), endDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(wsName, ParemontWrapper.DataProperty.HAS_ACTUAL_START_DATE_TIME.getShortName(), startDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(wsName, ParemontWrapper.DataProperty.HAS_ACTUAL_END_DATE_TIME.getShortName(), endDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addObjectPropertyValue(tmName, ParemontWrapper.ObjectProperty.HAS_WORK_SHIFT.getShortName(), wsName);
		owlWrapper.addObjectPropertyValue(wsName, ParemontWrapper.ObjectProperty.IS_WORK_SHIFT_OF.getShortName(), tmName);
		owlWrapper.addObjectPropertyValue(wsName, ParemontWrapper.ObjectProperty.INVOLVES_ROLE.getShortName(), role.getShortName());
		owlWrapper.addObjectPropertyValue(wsName, ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
		populateTeamMemberStatus(wsName, tmName, role, refInstant, day, shiftHours, onCall);
	}
	
	private int createWorkShiftsForAnyRole(Role role, int tmIndex, Instant refInstant, int day, int[] shiftHours) {
		for (int i = 0; i < role.getMinNumberPerShift(); i++) {
			final String tmName = role.getPotentialTeamMembers().get(tmIndex); 
			final String wsName = PREFIX_WORKSHIFT + role + "_" + tmName + "_D" + day + "_H" + shiftHours[0]; 
			createWorkShift(wsName, tmName, role, refInstant, day, shiftHours, false);
			tmIndex = (tmIndex + 1) % role.getPotentialTeamMembers().size();
		}
		// Add the "on call" team member
		final String tmName = role.getPotentialTeamMembers().get(tmIndex); 
		final String wsName = PREFIX_WORKSHIFT + "ONCALL_" + role + "_" + tmName + "_D" + day + "_H" + shiftHours[0]; 
		createWorkShift(wsName, tmName, role, refInstant, day, shiftHours, true);
		return (tmIndex + 1) % role.getPotentialTeamMembers().size();
	}
	
	private int createWorkShiftsForAgents(int tmIndex, Instant refInstant, int day, int[] shiftHours) {
		final Role role = Role.AGENT;
		for (Area area : Area.values()) {
			for (int i = 0; i < area.getnAgents(); i++) {
				final String tmName = role.getPotentialTeamMembers().get(tmIndex); 
				final String wsName = PREFIX_WORKSHIFT + role + "_" + tmName + "_D" + day + "_H" + shiftHours[0]; 
				createWorkShift(wsName, tmName, role, refInstant, day, shiftHours, false);
				owlWrapper.addObjectPropertyValue(wsName, ParemontWrapper.ObjectProperty.HAS_ASSIGNED_AREA.getShortName(), area.getShortName());
				tmIndex = (tmIndex + 1) % role.getPotentialTeamMembers().size();
			}
		}
		// Add the "on call" team member
		final String tmName = role.getPotentialTeamMembers().get(tmIndex); 
		final String wsName = PREFIX_WORKSHIFT + "ONCALL_" + role + "_" + tmName + "_D" + day + "_H" + shiftHours[0]; 
		createWorkShift(wsName, tmName, role, refInstant, day, shiftHours, true);
		return (tmIndex + 1) % role.getPotentialTeamMembers().size();
	}
	
	public void createWorkShifts(Instant refInstant) {
		final TreeMap<Role, Integer> nextAvailableTeamMember = new TreeMap<>();
		for (Role role : Role.values()) {
			nextAvailableTeamMember.put(role, 0);
		}
		for (int day = 0; day < DEF_TOTAL_DAYS; day++) {
			for (int[] shiftHours : DEF_SHIFTS) {
				for (Role role : Role.values()) {
					int tmIndex = nextAvailableTeamMember.get(role);
					if (Role.AGENT.equals(role)) {
						tmIndex = createWorkShiftsForAgents(tmIndex, refInstant, day, shiftHours);						
					}
					else {
						tmIndex = createWorkShiftsForAnyRole(role, tmIndex, refInstant, day, shiftHours);
					}
					nextAvailableTeamMember.put(role, tmIndex);
				}
			}
		}
	}

	private void createBooking(String servName, String prmName, Instant today) {
		final String bookName = PREFIX_BOOKING + servName;
		owlWrapper.addIndividual(ParemontWrapper.Clazz.BOOKING.getShortName(), bookName);
		owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.HAS_BOOKING.getShortName(), bookName);
		owlWrapper.addObjectPropertyValue(bookName, ParemontWrapper.ObjectProperty.HAS_ASSIGNED_SERVICE.getShortName(), servName);
		owlWrapper.addObjectPropertyValue(bookName, ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
		final double daysInAdvance = RND_DAYS_BEFORE_BOOKING_SERVICES.generate();
		final String bookingDate = adjustDateTime(today, (int)Math.floor(-daysInAdvance), 12).toString();
		owlWrapper.addDataPropertyValue(bookName, ParemontWrapper.DataProperty.HAS_BOOKING_DATE.getShortName(), bookingDate, OWL2Datatype.XSD_DATE_TIME);
	}
	
	private void createPRM(String servName, String prmName) {
		// Creates the associated PRM
		owlWrapper.addIndividual(ParemontWrapper.Clazz.PRM.getShortName(), prmName);
		// Assigns a random SSR Code
		owlWrapper.addObjectPropertyValue(prmName, ParemontWrapper.ObjectProperty.HAS_SSRCODE.getShortName(), SSRCode.values()[RND_SSR_CODE.generateInt()].getShortName());
		// Assigns PRM to service
		owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.HAS_PRM.getShortName(), prmName);
		// and assigns service to PRM
		owlWrapper.addObjectPropertyValue(prmName, ParemontWrapper.ObjectProperty.HAS_SERVICE.getShortName(), servName);
	}
	
	private void createPRMAndDepartureServices(String fName, Instant today) {
		final int nServices = RND_N_SERVICES_PER_FLIGHT.generateInt();
		for (int i = 0; i < nServices; i++) {
			final String servName = PREFIX_SERVICE + fName + "_" + i;
			final String prmName = PREFIX_PRM + fName + "_" + i;
			// Creates the service
			owlWrapper.addIndividual(ParemontWrapper.Clazz.DEPARTURE_SERVICE.getShortName(), servName);
			owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
			owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.HAS_ASSOCIATED_FLIGHT.getShortName(), fName);
			createPRM(servName, prmName);
			// Booking?
			final boolean booked = rnd.draw() < RND_PROP_BOOKED_SERVICES;
			if (booked) {
				createBooking(servName, prmName, today);
			}
		}
	}
	
	private void createPRMAndArrivalServices(String fName) {
		final int nServices = RND_N_SERVICES_PER_FLIGHT.generateInt();
		for (int i = 0; i < nServices; i++) {
			final String servName = PREFIX_SERVICE + fName + "_" + i;
			final String prmName = PREFIX_PRM + fName + "_" + i;
			// Creates the service
			owlWrapper.addIndividual(ParemontWrapper.Clazz.ARRIVAL_SERVICE.getShortName(), servName);
			owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.BELONGS_TO_AIRPORT.getShortName(), airportName);
			owlWrapper.addObjectPropertyValue(servName, ParemontWrapper.ObjectProperty.HAS_ASSOCIATED_FLIGHT.getShortName(), fName);
			createPRM(servName, prmName);
		}
	}

	private void createDelay(String fName, int duration) {
		String dName = PREFIX_DELAY + fName;
		owlWrapper.addIndividual(ParemontWrapper.Clazz.DELAY.getShortName(), dName);		
		owlWrapper.addDataPropertyValue(dName, ParemontWrapper.DataProperty.HAS_DURATION.getShortName(), "" + duration, OWL2Datatype.XSD_INTEGER);
		owlWrapper.addObjectPropertyValue(dName, ParemontWrapper.ObjectProperty.HAS_DELAY_CODE.getShortName(), DelayCode.CODE_19.getShortName());
		owlWrapper.addObjectPropertyValue(fName, ParemontWrapper.ObjectProperty.HAS_FLIGHT_INCIDENT.getShortName(), dName);
		owlWrapper.addObjectPropertyValue(dName, ParemontWrapper.ObjectProperty.IS_FLIGHT_INCIDENT_OF.getShortName(), fName);
	}
	
	private void createArrivalFlight(String fName, String startDate, Area gate) {
		owlWrapper.addIndividual(ParemontWrapper.Clazz.FLIGHT.getShortName(), fName);		
		owlWrapper.addDataPropertyValue(fName, ParemontWrapper.DataProperty.HAS_STA.getShortName(), startDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(fName, ParemontWrapper.ObjectProperty.HAS_ASSIGNED_AIRPORT_FACILITY_FLIGHT.getShortName(), gate.getShortName());
		owlWrapper.addObjectPropertyValue(fName, ParemontWrapper.ObjectProperty.HAS_DESTINATION_AIRPORT.getShortName(), airportName);
	}
	
	private void createDepartureFlight(String fName, String startDate, Area gate) {
		owlWrapper.addIndividual(ParemontWrapper.Clazz.FLIGHT.getShortName(), fName);		
		owlWrapper.addDataPropertyValue(fName, ParemontWrapper.DataProperty.HAS_STD.getShortName(), startDate, OWL2Datatype.XSD_DATE_TIME);
		owlWrapper.addDataPropertyValue(fName, ParemontWrapper.ObjectProperty.HAS_ASSIGNED_AIRPORT_FACILITY_FLIGHT.getShortName(), gate.getShortName());
		owlWrapper.addObjectPropertyValue(fName, ParemontWrapper.ObjectProperty.HAS_ORIGIN_AIRPORT.getShortName(), airportName);
	}
	
	public void createFlights(Instant refInstant) {
		// For each day
		int total = 0;
		System.out.println("FLIGHT\tARR_DAY\tARR_TIME\tDEP_DAY\tDEP_TIME\tGATE");
		for (int day = 0; day < DEF_TOTAL_DAYS; day++) {
			for (ArrivalFlightDistribution dist : ARRIVAL_FLIGHTS) {
				final int nFlights = ((DiscreteRandomVariate)dist.getnFlights()).generateInt();
				final double timeInterval = (dist.getEndingHour() - dist.getStartingHour()) / (double)nFlights;
				for (int i = 0; i < nFlights; i++) {
					final Area gate = (total % 3 == 0) ? Area.GATE_B : Area.GATE_A;
					final String arrivalFlightName = PREFIX_FLIGHT + "A_D" + day + "_H" + dist.getStartingHour() + "_" + dist.getEndingHour() + "_N" + i;
					final int arrivalHour = (int) (dist.getStartingHour() + timeInterval * i);
					final int minute = (int) ((timeInterval * i - (int) (timeInterval * i)) * 60);
					final Instant arrivalInstant = adjustDateTime(refInstant, day, arrivalHour, minute);
					createArrivalFlight(arrivalFlightName, arrivalInstant.toString(), gate);
					createPRMAndArrivalServices(arrivalFlightName);
					// Creates the corresponding departure flight
					final String departureFlightName = PREFIX_FLIGHT + "D_D" + day + "_H" + dist.getStartingHour() + "_" + dist.getEndingHour() + "_N" + i;
					int departureHour = arrivalHour + 1;
					int departureDay = day;
					if (departureHour >= DEF_ENDING_HOUR) {
						departureHour = DEF_STARTING_HOUR;
						departureDay = (departureDay + 1) % DEF_TOTAL_DAYS;
					}
					final Instant departureInstant = adjustDateTime(refInstant, departureDay, departureHour, minute);
					createDepartureFlight(departureFlightName, departureInstant.toString(), gate);
					createPRMAndDepartureServices(departureFlightName, departureInstant);
					final boolean delayed = rnd.draw() < PROP_DELAYED_FLIGHTS; 
					if (delayed) {
						createDelay(departureFlightName, RND_DELAY_TIME.generateInt());
					}
					System.out.println(arrivalFlightName + "\t" + day + "\t" + arrivalHour + ":" +  minute + "\t" + departureDay + "\t" + departureHour + ":" +  minute + "\t" + gate);
					total++;
				}
			}
		}
	}
	
	private static Instant adjustDateTime(Instant refInstant, int offsetDays, int hour) {
		return refInstant.atZone(ZoneOffset.UTC).plusDays(offsetDays).withHour(hour).truncatedTo(ChronoUnit.HOURS).toInstant();
	}
	
	private static Instant adjustDateTime(Instant refInstant, int offsetDays, int hour, int minute) {
		return refInstant.atZone(ZoneOffset.UTC).plusDays(offsetDays).withHour(hour).withMinute(minute).truncatedTo(ChronoUnit.MINUTES).toInstant();
	}
	
	public void saveChanges() throws OWLOntologyStorageException {
		owlWrapper.save();
	}
	
	@SuppressWarnings("unused")
	private void testAddingIndividual() throws OWLOntologyStorageException {
		owlWrapper.addIndividual(ParemontWrapper.Clazz.TEAM_MEMBER.getShortName(), "Paquito");
		owlWrapper.save();
	}

	@SuppressWarnings("unused")
	private void testListingIndividual(ParemontWrapper.Clazz clazz) {
		for (String name : owlWrapper.getIndividuals(clazz.getShortName())) {
			System.out.println("INDIVIDUAL: " + name);
			final ArrayList<String> props = owlWrapper.getIndividualProperties(name, ":");
			for (String prop : props)
				System.out.println("\t" + prop);
		}
	}
	
	@SuppressWarnings("unused")
	private void testAddingWorkShifts() {
		final Instant refInstant = Instant.now();
		createAreas();
		createTeamMembers();
		createWorkShifts(refInstant);
		testListingIndividual(ParemontWrapper.Clazz.TEAM_MEMBER);
		testListingIndividual(ParemontWrapper.Clazz.WORK_SHIFT);
	}

	@SuppressWarnings("unused")
	private void testAddingFlights() {
		final Instant refInstant = Instant.now();
		createFlights(refInstant);
		testListingIndividual(ParemontWrapper.Clazz.FLIGHT);
	}
		
	public void removeIndividuals(Set<ParemontWrapper.Clazz> clazzes) {
		for (ParemontWrapper.Clazz cl : clazzes) {
			owlWrapper.removeIndividualsOfClass(cl.getShortName());
		}
	}
	
	public void testRemoveAddedIndividuals() {
		removeIndividuals(EnumSet.of(ParemontWrapper.Clazz.TEAM_MEMBER, ParemontWrapper.Clazz.FLIGHT, ParemontWrapper.Clazz.WORK_SHIFT, 
				ParemontWrapper.Clazz.DEPARTURE_SERVICE, ParemontWrapper.Clazz.ARRIVAL_SERVICE,
				ParemontWrapper.Clazz.DELAY, ParemontWrapper.Clazz.PRM, ParemontWrapper.Clazz.BOOKING, ParemontWrapper.Clazz.TEMPORAL_STATUS,
				ParemontWrapper.Clazz.GATE, ParemontWrapper.Clazz.AIRPORT, ParemontWrapper.Clazz.CHECK_IN_AREA));

	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			InstanceGenerator gen = new InstanceGenerator("TestAirport");
//			gen.testRemoveAddedIndividuals();
			final Instant refInstant = Instant.now();
			gen.createAreas();
			gen.createTeamMembers();
			gen.createWorkShifts(refInstant);
			gen.createFlights(refInstant);
			gen.getOwlWrapper().printIndividuals(true);
			gen.saveChanges();
//		} catch (OWLOntologyCreationException e) {
		} catch (OWLOntologyCreationException | OWLOntologyStorageException e) {
			e.printStackTrace();
		}
	}

}
