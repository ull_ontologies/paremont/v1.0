**Table of Contents**  

- [PAREMONT](#paremont)
- [Authors](#authors)
- [Competency Questions](#competency-questions)
- [Generate test individuals tool](#test-tool)
- [Documentation](#documentation)


## PAREMONT

Nowadays, the management of persons with reduced mobility (PRMs) is an essential service in most airports around the world. At most airports, assistance involves at least three main stakeholders: (1) the airport management company in charge of supervising the execution of the service, (2) the company that provides the service, and (3) the airlines that manage special travel bookings. To offer a quality service, all these actors must be coordinated by sharing information, not only about the service itself, but also about other airport services such as resource allocation management (luggage carousels, check-in counters, boarding gates, etc.) and the management of flight incidents.The latest has a strong impact on the performance of the service, since they introduce uncertainties that may lead to delays in flight departures, with all the economic consequences that may arise from them.

One of the challenges to overcome the barriers in improving the current offer of PRM service management is through greater interconnection between the different information systems that intervene in the daily logistics of airports. Despite the benefits for the interconnection of information systems, there is no standard ontology that represents knowledge about PRM management in the context of airports. For this reason, and in an attempt to improve the quality of PRM services in airport facilities and optimize their performance, we present PAREMONT, which is an ontology expressed in OWL to represent knowledge about this domain.

## Authors

Juan José Herrera Martín (jhmartin@ull.edu.es) ORCID: 0000-0003-4915-7511 <br>
Gonçal Costa (goncal.costa@salle.url.edu) ORCID: 0000-0001-9168-680X <br>
Iván Castilla Rodríguez (icasrod@ull.edu.es) ORCID: 0000-0003-3933-2582 <br>
Evelio José González (ejgonzal@ull.edu.es) ORCID: 0000-0002-2203-3757 <br>

## Competency Questions 

The competency questions that served as the basis for the design of PAREMONT can be consulted in the file <b>CQs.xlsx</b>.

The CQs.xlsx file has several pages. The CQs page describes the Competency Questions organized by functional groups. The Data Properties and Objects Properties pages list the PAREMONT data and object properties and indicate the associated competency questions. The SparQL Querys page presents the example sparqls used in testing the ontology and the SparQL executions page presents the results of executing the queries described on the SparQL Querys page.

## Generate test individuals tool

The modelPMR folder contains the code of a tool that generates test individuals from a set of initial parameters. 

## Documentation 

The PAREMONT documentation was generated with Widoco and can be consulted [here]. The public folder contains the documentation.

[here]: https://ull_ontologies.gitlab.io/paremont/v1.0/

